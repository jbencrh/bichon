// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package source

import (
	"crypto/tls"
	"fmt"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/net"
)

type GitLab struct {
	Client *gitlab.Client
	Repo   *model.Repo
}

func NewGitLabForRepo(tlscfg *tls.Config, repo *model.Repo) (*GitLab, error) {
	server := fmt.Sprintf("https://%s/", repo.Server)

	client, err := gitlab.NewClient(repo.Token,
		gitlab.WithHTTPClient(net.NewHTTPClient(tlscfg)),
		gitlab.WithBaseURL(server))
	if err != nil {
		return nil, err
	}

	return &GitLab{
		Client: client,
		Repo:   repo,
	}, nil
}

func (gl *GitLab) Ping() error {
	_, _, err := gl.Client.Projects.GetProject(gl.Repo.Project, nil)
	if err != nil {
		return err
	}
	return nil
}

func (gl *GitLab) convertMergeRequest(glmreq *gitlab.MergeRequest) model.MergeReq {
	mreq := model.MergeReq{
		Repo:      *gl.Repo,
		ID:        uint(glmreq.IID),
		Title:     glmreq.Title,
		Labels:    glmreq.Labels,
		CreatedAt: *glmreq.CreatedAt,
		UpdatedAt: *glmreq.UpdatedAt,
		Submitter: model.Account{
			UserName: glmreq.Author.Username,
			RealName: glmreq.Author.Name,
		},
		Description:        glmreq.Description,
		State:              model.MergeReqState(glmreq.State),
		UpVotes:            glmreq.Upvotes,
		DownVotes:          glmreq.Downvotes,
		MergeStatus:        glmreq.MergeStatus,
		MergeAfterPipeline: glmreq.MergeWhenPipelineSucceeds,
		SourceBranch:       glmreq.SourceBranch,
		TargetBranch:       glmreq.TargetBranch,

		Metadata: model.MergeReqMetadata{
			Partial: true,
			Status:  model.STATUS_NEW,
		},
	}

	if glmreq.Assignee != nil {
		mreq.Assignee = &model.Account{
			UserName: glmreq.Assignee.Username,
			RealName: glmreq.Assignee.Name,
		}
	}

	return mreq
}

func (gl *GitLab) GetMergeRequests(onlyOpen bool, updatedAfter *time.Time) ([]model.MergeReq, error) {
	state := "all"
	if onlyOpen {
		log.Infof("Only getting open mreqs")
		state = "opened"
	} else {
		log.Infof("Getting all mreqs")
	}
	var pageidx int = 1
	var totalpages int = -1
	var mreqs []model.MergeReq = make([]model.MergeReq, 0)

	opts := &gitlab.ListProjectMergeRequestsOptions{
		State:       &state,
		ListOptions: gitlab.ListOptions{PerPage: 100, Page: pageidx},
	}
	if updatedAfter != nil {
		log.Infof("Filtering to those changed since %s", updatedAfter)
		opts.UpdatedAfter = updatedAfter
	}

	for pageidx <= totalpages || totalpages == -1 {
		glmreqs, resp, err := gl.Client.MergeRequests.ListProjectMergeRequests(
			gl.Repo.Project, opts)

		if err != nil {
			return []model.MergeReq{}, err
		}

		for _, glmreq := range glmreqs {
			mreqs = append(mreqs, gl.convertMergeRequest(glmreq))
		}
		if totalpages == -1 {
			totalpages, _ = strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		}
		pageidx++
	}
	return mreqs, nil
}

func (gl *GitLab) GetMergeRequest(mreq *model.MergeReq) (model.MergeReq, error) {
	glmreq, _, err := gl.Client.MergeRequests.GetMergeRequest(
		gl.Repo.Project, int(mreq.ID), nil)

	if err != nil {
		return model.MergeReq{}, err
	}

	return gl.convertMergeRequest(glmreq), nil
}

func (gl *GitLab) GetVersions(mreq *model.MergeReq) ([]model.Series, error) {
	var pageidx int = 1
	var totalpages int = -1
	vers := make([]model.Series, 0)

	for pageidx <= totalpages || totalpages == -1 {
		glvers, resp, err := gl.Client.MergeRequests.GetMergeRequestDiffVersions(
			gl.Repo.Project, int(mreq.ID),
			&gitlab.GetMergeRequestDiffVersionsOptions{
				PerPage: 100,
				Page:    pageidx,
			})
		if err != nil {
			return []model.Series{}, err
		}

		for idx, _ := range glvers {
			glver := glvers[len(glvers)-idx-1]
			ver := model.Series{
				Index:     idx + 1,
				Version:   glver.ID,
				BaseHash:  glver.BaseCommitSHA,
				HeadHash:  glver.HeadCommitSHA,
				StartHash: glver.StartCommitSHA,

				Metadata: model.SeriesMetadata{
					Partial: true,
				},
			}

			vers = append(vers, ver)
		}

		if totalpages == -1 {
			totalpages, _ = strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		}
		pageidx++
	}

	return vers, nil
}

func (gl *GitLab) GetPatches(mreq *model.MergeReq, series *model.Series) ([]model.Commit, error) {
	glver, _, err := gl.Client.MergeRequests.GetSingleMergeRequestDiffVersion(
		gl.Repo.Project, int(mreq.ID), series.Version, nil,
	)
	if err != nil {
		return []model.Commit{}, err
	}

	commits := make([]model.Commit, 0)

	for idx, _ := range glver.Commits {
		glcommit := glver.Commits[len(glver.Commits)-idx-1]
		commit := model.Commit{
			Hash:  glcommit.ID,
			Title: glcommit.Title,
			Author: model.User{
				Name:  glcommit.AuthorName,
				Email: glcommit.AuthorEmail,
			},
			Committer: model.User{
				Name:  glcommit.CommitterName,
				Email: glcommit.CommitterEmail,
			},
			CreatedAt: *glcommit.AuthoredDate,
			UpdatedAt: *glcommit.CommittedDate,
			Message:   glcommit.Message,

			Metadata: model.CommitMetadata{
				Partial: true,
			},
		}

		commits = append(commits, commit)
	}

	return commits, nil
}

func (gl *GitLab) GetCommitDiffs(mreq *model.MergeReq, commit *model.Commit) ([]model.Diff, error) {
	var pageidx = 1
	var totalpages = -1
	var diffs []model.Diff

	for pageidx <= totalpages || totalpages == -1 {
		gldiffs, resp, err := gl.Client.Commits.GetCommitDiff(
			gl.Repo.Project, commit.Hash,
			&gitlab.GetCommitDiffOptions{PerPage: 100, Page: pageidx}, nil)
		if err != nil {
			return nil, err
		}

		if err == nil {
			for _, gldiff := range gldiffs {
				diff := model.Diff{
					Content:     gldiff.Diff,
					NewFile:     gldiff.NewPath,
					OldFile:     gldiff.OldPath,
					NewMode:     gldiff.AMode,
					OldMode:     gldiff.BMode,
					CreatedFile: gldiff.NewFile,
					DeletedFile: gldiff.DeletedFile,
					RenamedFile: gldiff.RenamedFile,
				}
				diffs = append(diffs, diff)
			}
		}

		if totalpages == -1 {
			totalpages, _ = strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		}
		pageidx++

	}
	return diffs, nil
}

func (gl *GitLab) GetMergeRequestComments(mreq *model.MergeReq) ([]model.Comment, error) {
	var pageidx = 1
	var totalpages = -1
	comments := make([]model.Comment, 0)

	for pageidx <= totalpages || totalpages == -1 {
		glnotes, resp, err := gl.Client.Notes.ListMergeRequestNotes(
			gl.Repo.Project, int(mreq.ID),
			&gitlab.ListMergeRequestNotesOptions{
				ListOptions: gitlab.ListOptions{PerPage: 100,
					Page: pageidx},
			}, nil)
		if err != nil {
			return []model.Comment{}, err
		}

		for _, glnote := range glnotes {
			comment := model.Comment{
				CreatedAt: *glnote.CreatedAt,
				UpdatedAt: *glnote.UpdatedAt,
				Author: model.User{
					Name:  glnote.Author.Name,
					Email: glnote.Author.Email,
				},
				Description: glnote.Body,
				System:      glnote.System,
			}

			if glnote.Position != nil && glnote.Position.PositionType == "text" {
				comment.Context = &model.CommentContext{
					BaseHash:  glnote.Position.BaseSHA,
					HeadHash:  glnote.Position.HeadSHA,
					StartHash: glnote.Position.StartSHA,
					NewFile:   glnote.Position.NewPath,
					NewLine:   uint(glnote.Position.NewLine),
					OldFile:   glnote.Position.OldPath,
					OldLine:   uint(glnote.Position.OldLine),
				}
			}

			comments = append(comments, comment)
		}

		if totalpages == -1 {
			totalpages, _ = strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		}
		pageidx++
	}

	return comments, nil
}

func (gl *GitLab) GetMergeRequestThreads(mreq *model.MergeReq) ([]model.CommentThread, error) {
	var pageidx = 1
	var totalpages = -1
	threads := make([]model.CommentThread, 0)

	for pageidx <= totalpages || totalpages == -1 {
		gldisc, resp, err := gl.Client.Discussions.ListMergeRequestDiscussions(
			gl.Repo.Project, int(mreq.ID),
			&gitlab.ListMergeRequestDiscussionsOptions{
				PerPage: 100, Page: pageidx},
		)
		if err != nil {
			return []model.CommentThread{}, err
		}

		for _, gldisc := range gldisc {
			thread := model.CommentThread{
				ID:         gldisc.ID,
				Individual: gldisc.IndividualNote,
			}

			for _, glnote := range gldisc.Notes {
				comment := model.Comment{
					CreatedAt: *glnote.CreatedAt,
					UpdatedAt: *glnote.UpdatedAt,
					Author: model.User{
						Name:  glnote.Author.Name,
						Email: glnote.Author.Email,
					},
					Description: glnote.Body,
					System:      glnote.System,
				}

				if glnote.Position != nil && glnote.Position.PositionType == "text" {
					comment.Context = &model.CommentContext{
						BaseHash:  glnote.Position.BaseSHA,
						HeadHash:  glnote.Position.HeadSHA,
						StartHash: glnote.Position.StartSHA,
						NewFile:   glnote.Position.NewPath,
						NewLine:   uint(glnote.Position.NewLine),
						OldFile:   glnote.Position.OldPath,
						OldLine:   uint(glnote.Position.OldLine),
					}
				}

				thread.Comments = append(thread.Comments, comment)
			}
			threads = append(threads, thread)
		}

		if totalpages == -1 {
			totalpages, _ = strconv.Atoi(resp.Header.Get("X-Total-Pages"))
		}
		pageidx++
	}

	return threads, nil
}

func (gl *GitLab) AddMergeRequestComment(mreq *model.MergeReq, text string) error {
	glnote := gitlab.CreateMergeRequestNoteOptions{
		Body: &text,
	}
	_, _, err := gl.Client.Notes.CreateMergeRequestNote(
		gl.Repo.Project, int(mreq.ID), &glnote, nil)

	return err
}

func (gl *GitLab) AddMergeRequestThread(mreq *model.MergeReq, text string, context *model.CommentContext) error {
	gldiscuss := gitlab.CreateMergeRequestDiscussionOptions{
		Body: &text,
	}
	if context != nil {
		gldiscuss.Position = &gitlab.NotePosition{
			PositionType: "text",
			BaseSHA:      context.BaseHash,
			StartSHA:     context.StartHash,
			HeadSHA:      context.HeadHash,
			NewPath:      context.NewFile,
			NewLine:      int(context.NewLine),
			OldPath:      context.OldFile,
			OldLine:      int(context.OldLine),
		}

	}
	_, _, err := gl.Client.Discussions.CreateMergeRequestDiscussion(
		gl.Repo.Project, int(mreq.ID), &gldiscuss, nil)

	return err
}

func (gl *GitLab) AddMergeRequestReply(mreq *model.MergeReq, thread, text string) error {
	glnote := gitlab.AddMergeRequestDiscussionNoteOptions{
		Body: &text,
	}

	_, _, err := gl.Client.Discussions.AddMergeRequestDiscussionNote(
		gl.Repo.Project, int(mreq.ID), thread, &glnote, nil)

	return err
}

func (gl *GitLab) AcceptMergeRequest(mreq *model.MergeReq, removeSource bool) error {
	ver := mreq.Versions[len(mreq.Versions)-1]
	glaccept := gitlab.AcceptMergeRequestOptions{
		ShouldRemoveSourceBranch: &removeSource,
		SHA:                      &ver.HeadHash,
	}
	_, _, err := gl.Client.MergeRequests.AcceptMergeRequest(
		gl.Repo.Project, int(mreq.ID), &glaccept, nil)

	return err
}

func (gl *GitLab) ApproveMergeRequest(mreq *model.MergeReq) error {
	ver := mreq.Versions[len(mreq.Versions)-1]
	glaccept := gitlab.ApproveMergeRequestOptions{
		SHA: &ver.HeadHash,
	}
	_, _, err := gl.Client.MergeRequestApprovals.ApproveMergeRequest(
		gl.Repo.Project, int(mreq.ID), &glaccept, nil)

	return err
}

func (gl *GitLab) UnapproveMergeRequest(mreq *model.MergeReq) error {
	_, err := gl.Client.MergeRequestApprovals.UnapproveMergeRequest(
		gl.Repo.Project, int(mreq.ID), nil)

	return err
}

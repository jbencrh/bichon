// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"time"

	"github.com/gdamore/tcell"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type VersionFormListener interface {
	VersionFormConfirm(version int)
	VersionFormCancel()
}

type VersionForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener VersionFormListener

	Version           *tview.DropDown
	VersionNum        int
	VersionSearchNum  int
	VersionSearchLast time.Time
}

func (form *VersionForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.VersionFormCancel()
	form.Version.SetCurrentOption(form.VersionNum)
}

func (form *VersionForm) confirmFunc() {
	form.Form.SetFocus(0)
	idx, _ := form.Version.GetCurrentOption()
	log.Infof("Notifying version %d", idx)
	form.Listener.VersionFormConfirm(idx)
}

func NewVersionForm(listener VersionFormListener) *VersionForm {

	form := &VersionForm{
		Form:     tview.NewForm(),
		Listener: listener,
	}

	form.Primitive = Modal(form.Form, 30, 7)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.SetBorder(true).
		SetTitle("Patch series version")

	form.Version = addDropDown(form.Form, "Version", []string{"Latest"}, false)
	form.Version.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEnter:
			form.confirmFunc()
			return nil
		case tcell.KeyRune:
			switch event.Rune() {
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
				now := time.Now()
				if now.Sub(form.VersionSearchLast) > time.Second {
					form.VersionSearchNum = 0
				} else {
					form.VersionSearchNum *= 10
				}
				form.VersionSearchLast = now
				form.VersionSearchNum += int(event.Rune()) - int('0')
				form.Version.SetCurrentOption(form.VersionSearchNum)
				return nil
			}
		}
		return event
	})
	form.Form.AddButton("Apply", form.confirmFunc)
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *VersionForm) refreshMain(mreq *model.MergeReq) {
	versions := make([]string, len(mreq.Versions)+1)
	versions[0] = "Latest"
	for idx, _ := range mreq.Versions {
		versions[idx+1] = fmt.Sprintf("v%d", idx+1)
	}
	form.Version.SetOptions(versions, nil)
}

func (form *VersionForm) Refresh(app *tview.Application, mreq *model.MergeReq) {
	go app.QueueUpdateDraw(func() {
		form.refreshMain(mreq)
	})
}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"os"
	"path"
	"strings"
	"time"

	"github.com/go-ini/ini"
	log "github.com/sirupsen/logrus"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
)

type RepoState string

const (
	RepoStateActive   = RepoState("active")
	RepoStateInactive = RepoState("inactive")
	RepoStateHidden   = RepoState("hidden")
)

type Repo struct {
	Directory   string
	Remote      string
	Server      string
	Project     string
	Token       string
	GlobalToken bool
	State       RepoState

	UpdatedAt *time.Time
}

func (repo *Repo) String() string {
	return repo.Server + "/" + repo.Project
}

func (repo *Repo) Equal(otherrepo *Repo) bool {
	return repo.Server == otherrepo.Server && repo.Project == otherrepo.Project
}

func findGitDir(dir string) (string, error) {
	log.Infof("Find git dir %s", dir)
	gitdir := path.Join(dir, ".git")
	_, err := os.Stat(gitdir)
	if err != nil {
		if !os.IsNotExist(err) {
			return "", err
		}
		parent := path.Dir(dir)
		if parent == "/" {
			return "", fmt.Errorf("No git repo in current directory or its parents")
		}
		return findGitDir(parent)
	}

	return dir, nil
}

func FindGitDir() (string, error) {
	here, err := os.Getwd()
	if err != nil {
		return "", err
	}
	return findGitDir(here)
}

func loadRemote(directory, remote string) (string, string, error) {
	gitcfg := path.Join(directory, ".git", "config")

	cfg, err := ini.Load(gitcfg)
	if err != nil {
		return "", "", err
	}

	remoteStr := fmt.Sprintf("remote \"%s\"", remote)
	remoteURL := cfg.Section(remoteStr).Key("url").String()

	if remoteURL == "" {
		return "", "", fmt.Errorf("No remote '%s' found in %s",
			remote, gitcfg)
	}

	ep, err := transport.NewEndpoint(remoteURL)
	if err != nil {
		return "", "", err
	}

	if ep.Host == "" {
		return "", "", fmt.Errorf("Expected a hostname in git remote '%s'", remoteURL)
	}

	return ep.Host, strings.TrimPrefix(strings.TrimSuffix(ep.Path, ".git"), "/"), nil
}

func NewRepo(directory, remote, server, project, token string, globalToken bool, state RepoState) *Repo {
	repo := &Repo{
		Directory:   directory,
		Remote:      remote,
		Server:      server,
		Project:     project,
		Token:       token,
		GlobalToken: globalToken,
		State:       state,
	}

	log.Infof("Repo dir '%s' remote '%s' host '%s' project '%s' token '%s' globalToken '%t' state '%s'",
		repo.Directory, repo.Remote, repo.Server, repo.Project, "xxxxxxx", globalToken, state)

	return repo
}

func NewRepoForDirectory(directory, remote string) (*Repo, error) {
	server, project, err := loadRemote(directory, remote)
	if err != nil {
		return nil, err
	}

	return NewRepo(directory, remote, server, project, "", false, RepoStateActive), nil
}

type Repos []Repo

func (r Repos) Len() int {
	return len(r)
}

func (r Repos) Less(i, j int) bool {
	return strings.ToLower(r[i].String()) < strings.ToLower(r[j].String())
}

func (r Repos) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package filterstring

import (
	"testing"
)

func TestParse(t *testing.T) {
	filters := []string{
		"~p bichon",
		"~p bichon*",
		"~p */bichon",
		"~p bichon-project/bichon",
		"~p libvirt/libvirt-*",
		"~n Fred",
		"~n \"Fred Blogs\"",
		"~n 'Fred Blogs'",
		"~u fredblogs",
		"~a 1 month",
		"~t 1 month",
		"~s o",
		"!~p bichon",
		"~s open",
		"~m old",
		"~p bichon & ~n \"Fred Blogs\"",
		"~p bichon & ! ~u fredblogs",
		"~p bichon & ( ~u fredblogs | ~u joerandom) & ~s open",
		"(~p bichon | ~p libvirt) & ~t 1 month",
		"(~p bichon | ~p libvirt) & ~a 1 y",
		"(~p bichon | ~p libvirt) & ~t 1 month & ~m new",
	}

	for _, filter := range filters {
		expr, err := NewExpression(filter)
		if err != nil {
			t.Fatalf("cannot parse '%s': %s", filter, err)
		}

		_ = expr.BuildFilter()
	}
}

func TestStrings(t *testing.T) {
	filters := []string{
		"~p bichon",
		"~p 'bichon'",
		"~p \"bichon\"",
	}

	for _, filter := range filters {
		expr, err := NewExpression(filter)
		if err != nil {
			t.Fatalf("cannot parse '%s': %s", filter, err)
		}

		if expr.Left.Operator != nil {
			t.Fatalf("unexpected unary operator")
		}

		if expr.Left.Match.Project == nil {
			t.Fatalf("missing project match")
		}

		if expr.Left.Match.Project.AsString() != "bichon" {
			t.Fatalf("expected <bichon> got <%s>", expr.Left.Match.Project.AsString())
		}
	}
}

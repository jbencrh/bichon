// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package model

import (
	"testing"
)

func TestCommentContextRebase(t *testing.T) {
	mreqJSON := `{
  "id": 16,
  "title": "Test for comment rebase logic",
  "createdAt": "2020-06-02T13:54:33.829Z",
  "updatedAt": "2020-06-03T14:49:36.083Z",
  "submitter": {
    "username": "berrange",
    "realname": "Daniel Berrange"
  },
  "description": "",
  "versions": [
    {
      "index": 1,
      "version": 93578370,
      "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
      "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
      "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
      "patches": [
        {
          "hash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
          "title": "Starting content",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-06-01T16:20:29Z",
          "updatedAt": "2020-06-01T16:51:35Z",
          "message": "Starting content\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -0,0 +1,40 @@\n+file one line one\n+file one line two\n+file one line three\n+file one line four\n+file one line five\n+file one line six\n+file one line seven\n+file one line eight\n+file one line nine\n+file one line ten\n+file one line eleven\n+file one line twelve\n+file one line thirteen\n+file one line fourteen\n+file one line fifteen\n+file one line sixteen\n+file one line seventeen\n+file one line eighteen\n+file one line nineteen\n+file one line twenty\n+file one line twenty-one\n+file one line twenty-two\n+file one line twenty-three\n+file one line twenty-four\n+file one line twenty-five\n+file one line twenty-six\n+file one line twenty-seven\n+file one line twenty-eight\n+file one line twenty-nine\n+file one line thirty\n+file one line thirty-one\n+file one line thirty-two\n+file one line thirty-three\n+file one line thirty-four\n+file one line thirty-five\n+file one line thirty-six\n+file one line thirty-seven\n+file one line thirty-eight\n+file one line thirty-nine\n+file one line fourty\n",
              "newFile": "comments/file-one.txt",
              "oldFile": "comments/file-one.txt",
              "newMode": "0",
              "oldMode": "100644",
              "createdFile": true,
              "renamedFile": false,
              "deletedFile": false
            },
            {
              "content": "@@ -0,0 +1,40 @@\n+file three line one\n+file three line two\n+file three line three\n+file three line four\n+file three line five\n+file three line six\n+file three line seven\n+file three line eight\n+file three line nine\n+file three line ten\n+file three line eleven\n+file three line twelve\n+file three line thirteen\n+file three line fourteen\n+file three line fifteen\n+file three line sixteen\n+file three line seventeen\n+file three line eighteen\n+file three line nineteen\n+file three line twenty\n+file three line twenty-one\n+file three line twenty-two\n+file three line twenty-three\n+file three line twenty-four\n+file three line twenty-five\n+file three line twenty-six\n+file three line twenty-seven\n+file three line twenty-eight\n+file three line twenty-nine\n+file three line thirty\n+file three line thirty-one\n+file three line thirty-two\n+file three line thirty-three\n+file three line thirty-four\n+file three line thirty-five\n+file three line thirty-six\n+file three line thirty-seven\n+file three line thirty-eight\n+file three line thirty-nine\n+file three line fourty\n",
              "newFile": "comments/file-three.txt",
              "oldFile": "comments/file-three.txt",
              "newMode": "0",
              "oldMode": "100644",
              "createdFile": true,
              "renamedFile": false,
              "deletedFile": false
            },
            {
              "content": "@@ -0,0 +1,40 @@\n+file two line one\n+file two line two\n+file two line three\n+file two line four\n+file two line five\n+file two line six\n+file two line seven\n+file two line eight\n+file two line nine\n+file two line ten\n+file two line eleven\n+file two line twelve\n+file two line thirteen\n+file two line fourteen\n+file two line fifteen\n+file two line sixteen\n+file two line seventeen\n+file two line eighteen\n+file two line nineteen\n+file two line twenty\n+file two line twenty-one\n+file two line twenty-two\n+file two line twenty-three\n+file two line twenty-four\n+file two line twenty-five\n+file two line twenty-six\n+file two line twenty-seven\n+file two line twenty-eight\n+file two line twenty-nine\n+file two line thirty\n+file two line thirty-one\n+file two line thirty-two\n+file two line thirty-three\n+file two line thirty-four\n+file two line thirty-five\n+file two line thirty-six\n+file two line thirty-seven\n+file two line thirty-eight\n+file two line thirty-nine\n+file two line fourty\n",
              "newFile": "comments/file-two.txt",
              "oldFile": "comments/file-two.txt",
              "newMode": "0",
              "oldMode": "100644",
              "createdFile": true,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
          "title": "First change",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-06-01T16:21:31Z",
          "updatedAt": "2020-06-01T16:54:06Z",
          "message": "First change\n\nSigned-off-by: Daniel P. Berrangé \u003cberrange@redhat.com\u003e\n",
          "diffs": [
            {
              "content": "@@ -1,5 +1,5 @@\n file one line one\n-file one line two\n+file one line two alt\n file one line three\n file one line four\n file one line five\n@@ -7,8 +7,6 @@ file one line six\n file one line seven\n file one line eight\n file one line nine\n-file one line ten\n-file one line eleven\n file one line twelve\n file one line thirteen\n file one line fourteen\n@@ -21,8 +19,7 @@ file one line twenty\n file one line twenty-one\n file one line twenty-two\n file one line twenty-three\n-file one line twenty-four\n-file one line twenty-five\n+file one line twenty-miss\n file one line twenty-six\n file one line twenty-seven\n file one line twenty-eight\n@@ -30,7 +27,10 @@ file one line twenty-nine\n file one line thirty\n file one line thirty-one\n file one line thirty-two\n-file one line thirty-three\n+file one line thirty-four-a\n+file one line thirty-four-b\n+file one line thirty-four-c\n+file one line thirty-four-d\n file one line thirty-four\n file one line thirty-five\n file one line thirty-six\n@@ -38,3 +38,5 @@ file one line thirty-seven\n file one line thirty-eight\n file one line thirty-nine\n file one line fourty\n+file one line fourty-one\n+file one line fourty-two\n",
              "newFile": "comments/file-one.txt",
              "oldFile": "comments/file-one.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            },
            {
              "content": "@@ -1,5 +1,5 @@\n file three line one\n-file three line two\n+file three line two alt\n file three line three\n file three line four\n file three line five\n@@ -7,8 +7,6 @@ file three line six\n file three line seven\n file three line eight\n file three line nine\n-file three line ten\n-file three line eleven\n file three line twelve\n file three line thirteen\n file three line fourteen\n@@ -21,8 +19,7 @@ file three line twenty\n file three line twenty-one\n file three line twenty-two\n file three line twenty-three\n-file three line twenty-four\n-file three line twenty-five\n+file three line twenty-miss\n file three line twenty-six\n file three line twenty-seven\n file three line twenty-eight\n@@ -30,7 +27,10 @@ file three line twenty-nine\n file three line thirty\n file three line thirty-one\n file three line thirty-two\n-file three line thirty-three\n+file three line thirty-four-a\n+file three line thirty-four-b\n+file three line thirty-four-c\n+file three line thirty-four-d\n file three line thirty-four\n file three line thirty-five\n file three line thirty-six\n@@ -38,3 +38,5 @@ file three line thirty-seven\n file three line thirty-eight\n file three line thirty-nine\n file three line fourty\n+file three line fourty-one\n+file three line fourty-two\n",
              "newFile": "comments/file-three.txt",
              "oldFile": "comments/file-three.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            },
            {
              "content": "@@ -1,5 +1,5 @@\n file two line one\n-file two line two\n+file two line two alt\n file two line three\n file two line four\n file two line five\n@@ -7,8 +7,6 @@ file two line six\n file two line seven\n file two line eight\n file two line nine\n-file two line ten\n-file two line eleven\n file two line twelve\n file two line thirteen\n file two line fourteen\n@@ -21,8 +19,7 @@ file two line twenty\n file two line twenty-one\n file two line twenty-two\n file two line twenty-three\n-file two line twenty-four\n-file two line twenty-five\n+file two line twenty-miss\n file two line twenty-six\n file two line twenty-seven\n file two line twenty-eight\n@@ -30,7 +27,10 @@ file two line twenty-nine\n file two line thirty\n file two line thirty-one\n file two line thirty-two\n-file two line thirty-three\n+file two line thirty-four-a\n+file two line thirty-four-b\n+file two line thirty-four-c\n+file two line thirty-four-d\n file two line thirty-four\n file two line thirty-five\n file two line thirty-six\n@@ -38,3 +38,5 @@ file two line thirty-seven\n file two line thirty-eight\n file two line thirty-nine\n file two line fourty\n+file two line fourty-one\n+file two line fourty-two\n",
              "newFile": "comments/file-two.txt",
              "oldFile": "comments/file-two.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": false
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        },
        {
          "hash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
          "title": "Second change",
          "author": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "committer": {
            "name": "Daniel P. Berrangé",
            "email": "berrange@redhat.com"
          },
          "createdAt": "2020-06-01T17:03:53Z",
          "updatedAt": "2020-06-01T17:03:53Z",
          "message": "Second change\n",
          "diffs": [
            {
              "content": "@@ -0,0 +1,42 @@\n+file five line one\n+file five line two alt\n+file five line three\n+file five line four\n+file five line five\n+file five line six\n+file five line seven\n+file five line eight\n+file five line nine\n+file five line twelve\n+file five line thirteen\n+file five line fourteen\n+file five line fifteen\n+file five line sixteen\n+file five line seventeen\n+file five line eighteen\n+file five line nineteen\n+file five line twenty\n+file five line twenty-one\n+file five line twenty-two\n+file five line twenty-three\n+file five line twenty-miss\n+file five line twenty-six\n+file five line twenty-seven\n+file five line twenty-eight\n+file five line twenty-nine\n+file five line thirty\n+file five line thirty-one\n+file five line thirty-two\n+file five line thirty-four-a\n+file five line thirty-four-b\n+file five line thirty-four-c\n+file five line thirty-four-d\n+file five line thirty-four\n+file five line thirty-five\n+file five line thirty-six\n+file five line thirty-seven\n+file five line thirty-eight\n+file five line thirty-nine\n+file five line fourty\n+file five line fourty-one\n+file five line fourty-two\n",
              "newFile": "comments/file-five.txt",
              "oldFile": "comments/file-five.txt",
              "newMode": "0",
              "oldMode": "100644",
              "createdFile": true,
              "renamedFile": false,
              "deletedFile": false
            },
            {
              "content": "",
              "newFile": "comments/file-four.txt",
              "oldFile": "comments/file-two.txt",
              "newMode": "100644",
              "oldMode": "100644",
              "createdFile": false,
              "renamedFile": true,
              "deletedFile": false
            },
            {
              "content": "@@ -1,42 +0,0 @@\n-file three line one\n-file three line two alt\n-file three line three\n-file three line four\n-file three line five\n-file three line six\n-file three line seven\n-file three line eight\n-file three line nine\n-file three line twelve\n-file three line thirteen\n-file three line fourteen\n-file three line fifteen\n-file three line sixteen\n-file three line seventeen\n-file three line eighteen\n-file three line nineteen\n-file three line twenty\n-file three line twenty-one\n-file three line twenty-two\n-file three line twenty-three\n-file three line twenty-miss\n-file three line twenty-six\n-file three line twenty-seven\n-file three line twenty-eight\n-file three line twenty-nine\n-file three line thirty\n-file three line thirty-one\n-file three line thirty-two\n-file three line thirty-four-a\n-file three line thirty-four-b\n-file three line thirty-four-c\n-file three line thirty-four-d\n-file three line thirty-four\n-file three line thirty-five\n-file three line thirty-six\n-file three line thirty-seven\n-file three line thirty-eight\n-file three line thirty-nine\n-file three line fourty\n-file three line fourty-one\n-file three line fourty-two\n",
              "newFile": "comments/file-three.txt",
              "oldFile": "comments/file-three.txt",
              "newMode": "100644",
              "oldMode": "0",
              "createdFile": false,
              "renamedFile": false,
              "deletedFile": true
            }
          ],
          "bichonMetadata": {
            "partial": false
          }
        }
      ],
      "bichonMetadata": {
        "partial": false
      }
    }
  ],
  "threads": [
    {
      "id": "da2936b920f5b482de9c0a3e5f9ca573e0567b3b",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T13:56:03.573Z",
          "updatedt": "2020-06-02T13:56:03.573Z",
          "description": "Comment on file one line two, first change, new",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 2,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "f6e3567a55b399b11610ca28289ebdd5b9bbf688",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T13:58:21.283Z",
          "updatedt": "2020-06-02T13:58:21.283Z",
          "description": "Comment file one, line two, first change, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-one.txt",
            "newLine": 0,
            "oldFile": "comments/file-one.txt",
            "oldLine": 2
          }
        }
      ]
    },
    {
      "id": "b5265bf140eb23d631816dd8294a2e541ab6e7bc",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:03:56.016Z",
          "updatedt": "2020-06-02T14:03:56.016Z",
          "description": "Comment file one, line 10, first change, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-one.txt",
            "newLine": 0,
            "oldFile": "comments/file-one.txt",
            "oldLine": 10
          }
        }
      ]
    },
    {
      "id": "5e1f67bc3a29aa188fcaefe9d83e30bf92585e26",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:04:21.518Z",
          "updatedt": "2020-06-02T14:04:21.518Z",
          "description": "Comment file one, line 22, first change, new",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 22,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "2e2b01afaaf450373f8e3fb5f8c1deecd68a7292",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:04:34.861Z",
          "updatedt": "2020-06-02T14:04:34.861Z",
          "description": "Comment file one, first change, line24, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-one.txt",
            "newLine": 0,
            "oldFile": "comments/file-one.txt",
            "oldLine": 24
          }
        }
      ]
    },
    {
      "id": "5120bfed35640236adbff9133e4d9c9d114bb90c",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:04:49.099Z",
          "updatedt": "2020-06-02T14:04:49.099Z",
          "description": "Coment file one, first change, line 25 old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-one.txt",
            "newLine": 0,
            "oldFile": "comments/file-one.txt",
            "oldLine": 25
          }
        }
      ]
    },
    {
      "id": "e1bf8974fcee8da84c0392d23fc10754d1f8486d",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:05:11.423Z",
          "updatedt": "2020-06-02T14:05:11.423Z",
          "description": "Comemnt file one, first change, line 23, new",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 23,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "1a50b3edc11f04150461d627ea9adb60306cd101",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:05:24.219Z",
          "updatedt": "2020-06-02T14:05:24.219Z",
          "description": "comment file one, first change, line 26, old",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 23,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "f26d1708da7acd51323bb33187264a03b0330f7c",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:10:20.154Z",
          "updatedt": "2020-06-02T14:10:20.154Z",
          "description": "Comment file one, first change, line 32, new",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 32,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "446eac1d7367586548368bd14031c62cb2c6bd2c",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:10:49.013Z",
          "updatedt": "2020-06-02T14:10:49.013Z",
          "description": "Comment File three, first change, line 2, new",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 2,
            "oldFile": "comments/file-three.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "93f88a62b5b1950be54ef969186d3f03f12df284",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:11:05.113Z",
          "updatedt": "2020-06-02T14:11:05.113Z",
          "description": "Comment file three, first change,line 2, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 0,
            "oldFile": "comments/file-three.txt",
            "oldLine": 2
          }
        }
      ]
    },
    {
      "id": "dffe9476bca50c44bb8f08e51c33b61a22283785",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:17:37.444Z",
          "updatedt": "2020-06-02T14:17:37.444Z",
          "description": "Comment file three, first change, line 10, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 0,
            "oldFile": "comments/file-three.txt",
            "oldLine": 10
          }
        }
      ]
    },
    {
      "id": "61ec60303e5b37f68eb12cbaf084a7f2fb8f80b0",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:18:07.16Z",
          "updatedt": "2020-06-02T14:18:07.16Z",
          "description": "Comment file three, first change, line 24, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 0,
            "oldFile": "comments/file-three.txt",
            "oldLine": 24
          }
        }
      ]
    },
    {
      "id": "84d313df91b11adfacfb77250ac648b98c03c224",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:18:17.839Z",
          "updatedt": "2020-06-02T14:18:17.839Z",
          "description": "Comment file three, first change, line 22, new",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 22,
            "oldFile": "comments/file-three.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "4bcdd064a73137efe3be4d5da738302e33c8fabb",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:18:35.534Z",
          "updatedt": "2020-06-02T14:18:35.534Z",
          "description": "Comment file three, first change, line 31, new",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 31,
            "oldFile": "comments/file-three.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "00e8c770c129851a877683bb4cbf83e83ba0fcff",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:18:51.402Z",
          "updatedt": "2020-06-02T14:18:51.402Z",
          "description": "Comment file three, first change, line 41, new",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-three.txt",
            "newLine": 41,
            "oldFile": "comments/file-three.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "9397bfaec48eacbd221e0cea34d9372c384cb48a",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:19:08.813Z",
          "updatedt": "2020-06-02T14:19:08.813Z",
          "description": "Comment file two, first change, line 2, new",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-two.txt",
            "newLine": 2,
            "oldFile": "comments/file-two.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "f0b0c148510fe4500e550755da35c1b34d346a84",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:19:16.786Z",
          "updatedt": "2020-06-02T14:19:16.786Z",
          "description": "Comment file two, first change, line 2, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-two.txt",
            "newLine": 0,
            "oldFile": "comments/file-two.txt",
            "oldLine": 2
          }
        }
      ]
    },
    {
      "id": "87b1ef77f9f3344d9a0b53bc696ade38881af494",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:19:31.659Z",
          "updatedt": "2020-06-02T14:19:31.659Z",
          "description": "Comment file two, first change, line 10, old",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-two.txt",
            "newLine": 0,
            "oldFile": "comments/file-two.txt",
            "oldLine": 10
          }
        }
      ]
    },
    {
      "id": "36145491bb5403c9d76a864cf471e2c433645fed",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:19:51.057Z",
          "updatedt": "2020-06-02T14:19:51.057Z",
          "description": "Comment file two, first change, line 41, new",
          "system": false,
          "context": {
            "baseHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "startHash": "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
            "headHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "newFile": "comments/file-two.txt",
            "newLine": 41,
            "oldFile": "comments/file-two.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "285dbf31fe8918d67080209739cdb1f3b6160302",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:21:10.26Z",
          "updatedt": "2020-06-02T14:21:10.26Z",
          "description": "Comment file three, second change, line 19, old",
          "system": false,
          "context": {
            "baseHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "startHash": "31e825f6ef649357899aeea476dc3498d8a09e8e",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-three.txt",
            "newLine": 0,
            "oldFile": "comments/file-three.txt",
            "oldLine": 19
          }
        }
      ]
    },
    {
      "id": "2b519572d2c075f8d1ec722845cb2f263990834f",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:21:23.794Z",
          "updatedt": "2020-06-02T14:21:23.794Z",
          "description": "Comment file five, second change, line 15, new",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-five.txt",
            "newLine": 15,
            "oldFile": "comments/file-five.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "b7492ab1ef28a5948e9563fd6271df863ad6e411",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:36:54.323Z",
          "updatedt": "2020-06-02T14:36:54.323Z",
          "description": "Comment File one, first change, line 27 old",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 24,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "803312e72e66a21e266367371cd13fdd98e37d82",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-02T14:37:05.386Z",
          "updatedt": "2020-06-02T14:37:05.386Z",
          "description": "Comment File one, first change, line 24 new",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-one.txt",
            "newLine": 24,
            "oldFile": "comments/file-one.txt",
            "oldLine": 0
          }
        }
      ]
    },
    {
      "id": "d8608530ad105851efea8293a7b74597926145c2",
      "individual": false,
      "comments": [
        {
          "author": {
            "name": "Daniel Berrange",
            "email": ""
          },
          "createdAt": "2020-06-03T14:49:36.052Z",
          "updatedt": "2020-06-03T14:49:36.052Z",
          "description": "Comment n file four, second change, line 21, new (was file two)",
          "system": false,
          "context": {
            "baseHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "startHash": "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
            "headHash": "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
            "newFile": "comments/file-four.txt",
            "newLine": 21,
            "oldFile": "comments/file-four.txt",
            "oldLine": 0
          }
        }
      ]
    }
  ],
  "state": "opened",
  "labels": [],
  "bichonMetadata": {
    "partial": false,
    "status": "read"
  }
}`

	mreq, err := NewMergeReqFromJSON([]byte(mreqJSON))
	if err != nil {
		t.Fatalf("Cannot parse mreq json: %s", err)
	}

	expectedPatchContexts := [][]CommentContext{
		[]CommentContext{
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   2,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   10,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   24,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   25,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   26,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   26,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-three.txt",
				NewLine:   2,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-three.txt",
				NewLine:   10,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-three.txt",
				NewLine:   24,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-two.txt",
				NewLine:   2,
				OldFile:   "comments/file-two.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-two.txt",
				NewLine:   10,
				OldFile:   "comments/file-two.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				StartHash: "31e825f6ef649357899aeea476dc3498d8a09e8e",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-three.txt",
				NewLine:   21,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   27,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-one.txt",
				NewLine:   27,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				NewFile:   "comments/file-two.txt",
				NewLine:   23,
				OldFile:   "comments/file-four.txt",
				OldLine:   0,
			},
		},
		[]CommentContext{
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   2,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-one.txt",
				NewLine:   0,
				OldFile:   "comments/file-one.txt",
				OldLine:   2,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-one.txt",
				NewLine:   0,
				OldFile:   "comments/file-one.txt",
				OldLine:   10,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   22,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-one.txt",
				NewLine:   0,
				OldFile:   "comments/file-one.txt",
				OldLine:   24,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-one.txt",
				NewLine:   0,
				OldFile:   "comments/file-one.txt",
				OldLine:   25,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   23,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   23,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   32,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   2,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   0,
				OldFile:   "comments/file-three.txt",
				OldLine:   2,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   0,
				OldFile:   "comments/file-three.txt",
				OldLine:   10,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   0,
				OldFile:   "comments/file-three.txt",
				OldLine:   24,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   22,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   31,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   41,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-two.txt",
				NewLine:   2,
				OldFile:   "comments/file-two.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-two.txt",
				NewLine:   0,
				OldFile:   "comments/file-two.txt",
				OldLine:   2,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-two.txt",
				NewLine:   0,
				OldFile:   "comments/file-two.txt",
				OldLine:   10,
			},
			CommentContext{
				BaseHash:  "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				StartHash: "98b7b99a10dedee9e41b150b55542fbd3bc5e72a",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-two.txt",
				NewLine:   41,
				OldFile:   "comments/file-two.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				StartHash: "31e825f6ef649357899aeea476dc3498d8a09e8e",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-three.txt",
				NewLine:   19,
				OldFile:   "comments/file-three.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   24,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   24,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				NewFile:   "comments/file-two.txt",
				NewLine:   21,
				OldFile:   "comments/file-four.txt",
				OldLine:   0,
			},
		},
		[]CommentContext{
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   2,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   22,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   23,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   23,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   32,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "31e825f6ef649357899aeea476dc3498d8a09e8e",
				StartHash: "31e825f6ef649357899aeea476dc3498d8a09e8e",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-three.txt",
				NewLine:   0,
				OldFile:   "comments/file-three.txt",
				OldLine:   19,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-five.txt",
				NewLine:   15,
				OldFile:   "comments/file-five.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   24,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-one.txt",
				NewLine:   24,
				OldFile:   "comments/file-one.txt",
				OldLine:   0,
			},
			CommentContext{
				BaseHash:  "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				StartHash: "44c4f192fe950d587d9a7c96cca0f32bbe84ac23",
				HeadHash:  "7f7a3b74dc8e59bc662558e0ceca9611a31ef9b1",
				NewFile:   "comments/file-four.txt",
				NewLine:   21,
				OldFile:   "comments/file-four.txt",
				OldLine:   0,
			},
		},
	}

	series := mreq.Versions[0]

	for idx, patch := range series.Patches {
		actualContext := []CommentContext{}
		for _, thread := range mreq.Threads {
			origCtx := thread.Comments[0].Context

			newCtx := origCtx.Rebase(&patch, &series)
			if newCtx != nil {
				actualContext = append(actualContext, *newCtx)
			}
		}

		expectContext := expectedPatchContexts[idx]

		if len(expectContext) != len(actualContext) {
			t.Fatalf("Expected %d comments on patch %d but got %d",
				len(expectContext), idx+1, len(actualContext))
		}

		for cidx, _ := range expectContext {
			a := expectContext[cidx]
			b := actualContext[cidx]

			if a.NewFile != b.NewFile || a.NewLine != b.NewLine {
				t.Fatalf("Expected new %s:%d context %d patch %d but got %s:%d",
					a.NewFile, a.NewLine, cidx, idx, b.NewFile, b.NewLine)
			}
			if a.OldFile != b.OldFile || a.OldLine != b.OldLine {
				t.Fatalf("Expected old %s:%d context %d patch %d but got %s:%d",
					a.OldFile, a.OldLine, cidx, idx, b.OldFile, b.OldLine)
			}
		}
	}

}

// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package security

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"io"
	"runtime"

	"golang.org/x/crypto/argon2"
)

type MasterKeyArgon2Params struct {
	KeyLen       uint32
	Salt         []byte
	Time         uint32
	Memory       uint32
	Digest       []byte
	DigestSalt   []byte
	DigestTime   uint32
	DigestMemory uint32
}

func maxThreads() uint8 {
	numcpus := runtime.NumCPU()
	if numcpus > 255 {
		numcpus = 255
	}
	return uint8(numcpus)
}

func ValidateMasterKeyArgon2(password string, params *MasterKeyArgon2Params) ([32]byte, error) {
	possiblekey := argon2.IDKey([]byte(password), params.Salt, params.Time, params.Memory, maxThreads(), 32)
	possibledigest := argon2.IDKey(possiblekey, params.Salt, params.Time, params.Memory, maxThreads(), 32)

	if bytes.Compare(possibledigest, params.Digest) != 0 {
		return [32]byte{}, fmt.Errorf("Password does not match")
	}

	var masterkey [32]byte
	copy(masterkey[:], possiblekey[:32])

	return masterkey, nil
}

func generateSalt() ([]byte, error) {
	key := make([]byte, 32)
	_, err := io.ReadFull(rand.Reader, key)
	return key, err
}

func GenerateMasterKeyArgon2(password string) ([32]byte, *MasterKeyArgon2Params, error) {
	params := &MasterKeyArgon2Params{}

	salt, err := generateSalt()
	if err != nil {
		return [32]byte{}, nil, err
	}

	params.Salt = salt
	params.Time = 1
	params.Memory = 64 * 1024

	salt, err = generateSalt()
	if err != nil {
		return [32]byte{}, nil, err
	}

	params.DigestSalt = salt
	params.DigestTime = 1
	params.DigestMemory = 64 * 1024

	key := argon2.IDKey([]byte(password), params.Salt, params.Time, params.Memory, maxThreads(), 32)

	params.Digest = argon2.IDKey(key, params.Salt, params.Time, params.Memory, maxThreads(), 32)

	var masterkey [32]byte
	copy(masterkey[:], key[:32])

	return masterkey, params, nil
}

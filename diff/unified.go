// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package diff

import (
	"fmt"
	"strconv"
	"strings"
)

type DiffLineType int

const (
	DIFF_LINE_CONTEXT DiffLineType = iota
	DIFF_LINE_ADDED
	DIFF_LINE_REMOVED
)

type DiffLine struct {
	Type    DiffLineType
	Text    string
	OldLine uint
	NewLine uint
}

type DiffHunk struct {
	OldLine  uint
	OldCount uint
	NewLine  uint
	NewCount uint
	Section  string
	Lines    []DiffLine
}

func (hunk *DiffHunk) FormatContext() string {
	return fmt.Sprintf("@@ -%d,%d +%d,%d @@ %s",
		hunk.OldLine, hunk.OldCount, hunk.NewLine, hunk.NewCount, hunk.Section)
}

func parseLineNumbers(line, pair string) (uint, uint, error) {
	counters := strings.Split(pair, ",")

	if len(counters) > 2 {
		return 0, 0, fmt.Errorf("Too many fields in hunk context line numbers '%s'", line)
	}

	start, err := strconv.Atoi(counters[0])
	if err != nil {
		return 0, 0, fmt.Errorf("Non-numeric hunk context old line '%s': %s", line, err)
	}

	if len(counters) == 2 {
		count, err := strconv.Atoi(counters[1])
		if err != nil {
			return 0, 0, fmt.Errorf("Non-numeric hunk context old count '%s': %s", line, err)
		}
		return uint(start), uint(count), nil
	} else {
		return uint(start), 1, nil
	}
}

func parseUnifiedDiffContext(line string) (DiffHunk, error) {
	bits := strings.SplitN(line, " ", 5)
	if len(bits) < 4 {
		return DiffHunk{}, fmt.Errorf("Malformed hunk context '%s', too few fields", line)
	}

	if bits[0] != "@@" || bits[3] != "@@" {
		return DiffHunk{}, fmt.Errorf("Malformed hunk context '%s', missing @@", line)
	}

	if bits[1][0] != '-' {
		return DiffHunk{}, fmt.Errorf("Malformed hunk context '%s', missing '-'", line)
	}
	if bits[2][0] != '+' {
		return DiffHunk{}, fmt.Errorf("Malformed hunk context '%s', missing '+'", line)
	}

	var hunk DiffHunk
	var err error

	hunk.OldLine, hunk.OldCount, err = parseLineNumbers(line, bits[1][1:])
	if err != nil {
		return DiffHunk{}, err
	}
	hunk.NewLine, hunk.NewCount, err = parseLineNumbers(line, bits[2][1:])
	if err != nil {
		return DiffHunk{}, err
	}

	if len(bits) > 4 {
		hunk.Section = bits[4]
	}

	return hunk, nil
}

func ParseUnifiedDiffHunks(diff string) ([]DiffHunk, error) {
	lines := strings.Split(diff, "\n")
	lines = lines[0 : len(lines)-1]

	hunks := make([]DiffHunk, 0)
	oldLine := uint(0)
	newLine := uint(0)
	for _, line := range lines {
		if strings.HasPrefix(line, "@@") {
			hunk, err := parseUnifiedDiffContext(line)
			if err != nil {
				return []DiffHunk{}, err
			}
			hunks = append(hunks, hunk)
			oldLine = hunk.OldLine
			newLine = hunk.NewLine
		} else {
			if len(hunks) == 0 {
				return []DiffHunk{}, fmt.Errorf("Unable to parse patch diff '%s', missing context", line)
			}

			hunk := &hunks[len(hunks)-1]
			if strings.HasPrefix(line, "-") {
				hunk.Lines = append(hunk.Lines, DiffLine{
					Type:    DIFF_LINE_REMOVED,
					Text:    line,
					OldLine: oldLine,
				})
				oldLine++
			} else if strings.HasPrefix(line, "+") {
				hunk.Lines = append(hunk.Lines, DiffLine{
					Type:    DIFF_LINE_ADDED,
					Text:    line,
					NewLine: newLine,
				})
				newLine++
			} else {
				hunk.Lines = append(hunk.Lines, DiffLine{
					Type:    DIFF_LINE_CONTEXT,
					Text:    line,
					OldLine: oldLine,
					NewLine: newLine,
				})
				oldLine++
				newLine++
			}
		}
	}

	return hunks, nil
}
